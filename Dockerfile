FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1
ENV GLIBC_VERSION 2.23-r3

COPY ./ /quasi
COPY ./requirements.txt /requirements.txt

RUN apk add --update --no-cache python3-dev gcc musl-dev make
RUN pip install -r /requirements.txt

WORKDIR /quasi

ENTRYPOINT ["python", "./quasi/quasi.py"]
